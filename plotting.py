import os
import fnmatch
import csv
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
#import seaborn as sns
from matplotlib.pyplot import cm
import numpy as np
import math
#from matplotlib import rc
#import subprocess
import sys
from scipy.stats import tmean, scoreatpercentile
import collections

mod='ALL'
#mod='BX'

lumiSection=1
#lumiSection=4096
#lumiSection=16384
#lumiSection=32768
#lumiSection=262144

min=425
max=460
dict1={}
dict2={}
odict={}
with open('./out'+mod+'.csv') as f:
	fr = csv.reader(f,delimiter=',')
	ls = 1
	x=[]
	y=[]
	mc=0
	flag = 0
	counter=0
	for row in fr:
		if flag == 0:
			limMax = int(row[0]) + lumiSection
			flag = 1
		if int(row[0]) >= limMax:
			x.append(ls)
			y.append(mc)
			dict1[ls]=mc
			ls+=1
			mc=0
			limMax = int(row[0]) + lumiSection
		mc+=int(row[1])
		if counter == 200:
			break
		counter+=1
for k,v in dict1.items():
	ts = (k-440)*1.458
	nk = 1540292034 + ts
	dict2[nk] = v

with open('./DTRawRates.csv','wb') as dt:
	dtwriter = csv.writer(dt, delimiter=',')
	for k,v in dict2.items():
		dtwriter.writerow([k,v])

plt.errorbar(x, y, yerr=0, fmt='o')
#plt.xlim(min,max)
if mod=='BX':
	plt.title('Fill 7333 - Run 325117 - BX2381')
else:
	plt.title('Fill 7333 - Run 325117')
plt.ylabel("Number of muons")
#plt.xlabel("Orbits [$2^{14}$]")
plt.xlabel("Per Orbit")
plt.savefig( './'+mod+'_LS'+str(lumiSection)+'.pdf')
plt.clf()

					
