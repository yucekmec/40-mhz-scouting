# PATH=/nfshome0/lumipro/brilconda/bin/:$PATH 
import tables, pandas as pd, pylab as py, sys, numpy
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from math import sqrt
from datetime import datetime
filen = []
 
file1 = '/brildata/18/7333/7333_325117_1810230735_1810231111.hd5' 
filen.append(file1)
#file2 = '/brildata/18/7333/any.hd5' 
#filen.append(file2)
bxraw_list = [[] for a in range(253)] # this range should include colliding bx for which you will lot bxraw
timestamp_list =  []
for f in filen:
    h5file = tables.open_file(f)
    
    #this is detector table you look at, change if you want not PLT
    hist = h5file.root.pltlumizero 
    #hist = h5file.root.hfetlumi
    for row in hist.iterrows():
        #dt = datetime.fromtimestamp(row['timestampsec']) #converts to normal time
        #timestamp_list.append(dt)
        timestamp_list.append(row['timestampsec']) #time in timestamps directly
        for bx in range(253):
            bxraw_list[bx].append(row['bxraw'][bx])
            
# Plot fot the whole file rates
#plt.figure(facecolor="white")
#plt.plot_date(timestamp_list,bxraw_list[150],'.-') # use thi ploting if converted timestamts to time dt
plt.plot(timestamp_list,bxraw_list[150],'.-') # here change bx number for colliding bx in the fill you are looking at!
plt.grid()
plt.xlabel('Time', fontsize=16)
plt.ylabel('mu values', fontsize=16)
plt.title('Fill  7333, $\sqrt{s}$=13 TeV')
#plt.gcf().autofmt_xdate()
plt.show()

